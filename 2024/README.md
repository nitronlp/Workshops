# Workshops 2024 | Third Edition

## NLP Special
* Speaker: Ioan Ungureanu
* [GenAI in Real Life Presentation](https://docs.google.com/presentation/d/1bnmyUUnOXKsOok6Exn2Ik6IGhDvX8Msm/edit?usp=drive_link&ouid=104125965785644515865&rtpof=true&sd=true)

## Python & Data Visualization
* Speaker: Matei Simtinică
* [Exercises, Solutions](https://github.com/Nitro-Language-Processing/Workshops-2024/tree/main/Python%20and%20Data%20Visualisation)

## Machine Learning
* Speaker: Matei Simtinică
* [Toy Dataset, Exercises](https://github.com/Nitro-Language-Processing/Workshops-2024/tree/main/Machine%20Learning)

## Natural Language Processing
* Speaker: Miruna-Andreea Zăvelcă
* [Slides, Exercises, Solutions](https://github.com/Nitro-Language-Processing/Workshops-2024/tree/main/Natural%20Language%20Processing)

## Deep Learning
* Speaker: Antonio Bărbălău
* [Code Example](https://github.com/Nitro-Language-Processing/Workshops-2024/tree/main/Deep%20Learning)

## Transformers
* Speaker: Dana Dăscălescu
* [Theory, Code Example](https://github.com/Nitro-Language-Processing/Workshops-2024/tree/main/Transformers)
